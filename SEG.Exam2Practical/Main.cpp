
// Exam 2 Practical
// Seth Gilsczinski

#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>

using namespace std;

float Average(const float* pArray, const int size, float &answer)
{
	float sumTotal = 0;
	for (int i = 0; i < size; i++)
	{
		sumTotal = sumTotal + pArray[i];
	}
	answer = sumTotal / size;
	return answer;
}

int main()
{
	// Ask the user for 5 numbers
	const int SIZE = 5;
	float numbers[SIZE];
	cout << "Enter 5 numbers:";
	for (int i = 0; i < SIZE; i++)
	{
		cin >> numbers[i];
	}

	// Print to the console
	cout << "You entered: ";
	for (int i = 0; i < SIZE; i++)
	{
		cout << numbers[i];
		if (i < SIZE - 1) cout << ", ";
	}
	cout << "\n";

	// **Extra Credit: Print them in reverse
	cout << "Your numbers backwards are: ";
	for (int i = 4; i >= 0; i--)
	{
		cout << numbers[i];
		if (i > 0) cout << ", ";
	}
	cout << "\n";

	// Calculate average
	float answer;
	Average(numbers, SIZE, answer);
	cout << "The average of your numbers is " << answer << "\n";

	// Calculate Minimum
	float smallestNum = numbers[0];
	for (int i = 0; i < SIZE; i++)
	{
		if (smallestNum > numbers[i])
			smallestNum = numbers[i];	
	}
	cout << "Your smallest number is " << smallestNum << "\n";

	// Calculate Maximum
	float largestNum = numbers[0];
	for (int i = 0; i < SIZE; i++)
	{
		if (largestNum < numbers[i])
			largestNum = numbers[i];
	}
	cout << "Your largest number is " << largestNum << "\n";

	// Ask if they would like to save
	cout << "Would you like to save to a file? (y/n)";

	string input;
	cin >> input;

	if (input == "y")
	{
		string path = "C:\\Users\\public\\arrays.txt";
		ofstream ofs(path);

		ofs << "You entered: ";
		for (int i = 0; i < SIZE; i++)
		{
			ofs << numbers[i];
			if (i < SIZE - 1) ofs << ", ";
		}
		ofs << "\n";

		ofs << "Your numbers backwards are: ";
		for (int i = 4; i >= 0; i--)
		{
			ofs << numbers[i];
			if (i > 0) ofs << ", ";
		}
		ofs << "\n";

		ofs << "The average of your numbers is " << answer << "\n";

		ofs << "Your smallest number is " << smallestNum << "\n";

		ofs << "Your largest number is " << largestNum << "\n";
			
		ofs.close();

		cout << "Your array file has been saved!";
	}
	else
		cout << "Your array file has not been saved.";

	(void)_getch();
	return 0;
}
